package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Entity
public class MedicationPlan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    private String startDate;
    private String endDate;

    @OneToMany(targetEntity = Medication.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    List<Medication> meds;

    public MedicationPlan() {

    }

    public MedicationPlan(String startDate, String endDate, List<Medication> meds) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.meds = meds;
    }

    public MedicationPlan(UUID id, String startDate, String endDate, List<Medication> meds) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.meds = meds;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Transactional
    public List<Medication> getMeds() {
        return meds;
    }

    public void setMeds(List<Medication> meds) {
        this.meds = meds;
    }
}

