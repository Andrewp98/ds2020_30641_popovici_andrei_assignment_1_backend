package ro.tuc.ds2020.entities;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.List;

@Entity
public class Patient extends Person {

    private static final long serialVersionUID = 1L;

    @Column(name = "medical_record", nullable = true)
    private String medRec;

    @OneToMany(targetEntity = MedicationPlan.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<MedicationPlan> medicationPlans;

    public Patient() {

    }

    public Patient(String medRec) {
        this.medRec = medRec;
    }

    public Patient(String name, String address, int age, String medRec) {
        super(name, address, age);
        this.medRec = medRec;
    }

    public Patient(String name, String address, int age, String gender, String medRec) {
        super(name, address, age, gender);
        this.medRec = medRec;
    }

    public Patient(String name, String address, int age, String gender, String username, String password, String medRec) {
        super(name, address, age, gender, username, password);
        this.medRec = medRec;
    }

    public Patient(String name, String address, int age, String gender, String username, String password, String medRec, List<MedicationPlan> medicationPlans) {
        super(name, address, age, gender, username, password);
        this.medRec = medRec;
        this.medicationPlans = medicationPlans;
    }

    public String getMedRec() {
        return medRec;
    }

    public void setMedRec(String medRec) {
        this.medRec = medRec;
    }

    @Transactional
    public List<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(List<MedicationPlan> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }

}
