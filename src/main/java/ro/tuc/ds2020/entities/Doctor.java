package ro.tuc.ds2020.entities;

import javax.persistence.Entity;

@Entity
public class Doctor extends Person {

    public Doctor() {
    }

    public Doctor(String name, String address, int age) {
        super(name, address, age);
    }

    public Doctor(String name, String address, int age, String gender) {
        super(name, address, age, gender);
    }

    public Doctor(String name, String address, int age, String gender, String username, String password) {
        super(name, address, age, gender, username, password);
    }
}
