package ro.tuc.ds2020.entities;

import javax.persistence.*;
import java.util.List;

@Entity
public class Caregiver extends Person {

    private static final long serialVersionUID = 1L;

    @Column(name = "Takes_care_of", nullable = true)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "Caregiver_to_patient", joinColumns = @JoinColumn(name = "id"))
    private List<Patient> patList;

    public Caregiver() {

    }

    public Caregiver(String name, String address, int age, List<Patient> patList) {
        super(name, address, age);
        this.patList = patList;
    }

    public Caregiver(String name, String address, int age, String gender, List<Patient> patList) {
        super(name, address, age, gender);
        this.patList = patList;
    }

    public Caregiver(String name, String address, int age, String gender, String username, String password, List<Patient> patList) {
        super(name, address, age, gender, username, password);
        this.patList = patList;
    }

    public Caregiver(List<Patient> patList) {
        this.patList = patList;
    }

    public List<Patient> getPatList() {
        return patList;
    }

    public void setPatList(List<Patient> patList) {
        this.patList = patList;
    }
}
