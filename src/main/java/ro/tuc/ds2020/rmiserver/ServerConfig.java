package ro.tuc.ds2020.rmiserver;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;
import org.springframework.remoting.support.RemoteExporter;

import java.rmi.RMISecurityManager;
import java.rmi.Remote;

@Configuration
public class ServerConfig {

    @Bean
    MedPlanService medPlanService(){
        return new MedPlanServiceImpl();
    }

    @Bean
    RemoteExporter registerRMIExporter(MedPlanService implementation){
        Class<MedPlanService> serviceInterface = MedPlanService.class;
        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceInterface(serviceInterface);
        exporter.setService(implementation);
        exporter.setServiceName("MedPlanService");
        exporter.setRegistryPort(1099);
        return exporter;
    }
}
