package ro.tuc.ds2020.rmiserver;

import ro.tuc.ds2020.dtos.MedicationPlanDTO;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.UUID;

public interface MedPlanService /*extends Remote*/ {
//    public String getMedPlan(UUID id);
    public List<MedicationPlanDTO> getMedPlan(UUID id);

//    public void sendMessage(String text) throws RemoteException;
//
//    public String getMessage(String text) throws RemoteException;
}
