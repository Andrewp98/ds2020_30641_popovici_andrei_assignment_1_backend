package ro.tuc.ds2020.rmiserver;

import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.MedicationPlanService;
import ro.tuc.ds2020.services.PatientService;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MedPlanServiceImpl implements MedPlanService {

    @Autowired
    private PatientService patientService;

    @Autowired
    private MedicationPlanService medicationPlanService;

//    @Override
//    public void sendMessage(String s) throws RemoteException {
//        System.out.println(s);
//    }
//
//    @Override
//    public String getMessage(String text) throws RemoteException {
//        return "Your message is: " + text;
//    }

    @Override
    public List<MedicationPlanDTO> getMedPlan(UUID id){
        Patient patient = patientService.findPatientByIdNonDTO(id);
        List<MedicationPlan> medPlan = patient.getMedicationPlans();

        Medication medication = new Medication(UUID.randomUUID(), "NOSPA", "oboseala", Float.parseFloat("3.24"));
        List<Medication> medicationList = new ArrayList<>();
        medicationList.add(medication);
        MedicationPlan medPlanTemp = new MedicationPlan(UUID.randomUUID(), "07:00:00", "20:00:00", medicationList);
        medPlan.add(medPlanTemp);


//        UUID mpid = medicationPlanService.insert(medPlanTemp);
//        System.out.println(mpid);
//        patientService.addMedicationPlan(UUID.fromString("16eeb351-2691-4d6f-bcd7-2224f86c7a79"), mpid);

        List<MedicationPlanDTO> medPlanDTO = new ArrayList<>();
        for(MedicationPlan medicationPlan : medPlan){
            medPlanDTO.add(MedicationPlanBuilder.toMedicationPlanDTO(medicationPlan));
        }
        return medPlanDTO;
    }
}
