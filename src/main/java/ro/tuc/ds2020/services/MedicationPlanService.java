package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {

    public static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);

    @Autowired
    private MedicationPlanRepository medicationPlanRepository;

    @Autowired
    private MedicationService medicationService;

    public MedicationPlanDTO findById(UUID id){
        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(id);

        if(!medicationPlan.isPresent()){
            LOGGER.error("MedicationPlan with id {} was not found in db", id);
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + id);
        }

        return MedicationPlanBuilder.toMedicationPlanDTO(medicationPlan.get());
    }

    public List<MedicationPlanDTO> findAll(){
        List<MedicationPlan> medicationPlanList =  medicationPlanRepository.findAll();
        return  medicationPlanList.stream().map(MedicationPlanBuilder::toMedicationPlanDTO).collect(Collectors.toList());
    }

    public UUID insert(MedicationPlan medicationPlan){
        return medicationPlanRepository.save(medicationPlan).getId();
    }

    public void delete(UUID id){
        medicationPlanRepository.deleteById(id);
    }

    public UUID update(MedicationPlan medicationPlan) {
        Optional<MedicationPlan> tempMedicationPlan = medicationPlanRepository.findById(medicationPlan.getId());

        if(!tempMedicationPlan.isPresent()){
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + medicationPlan.getId());
        }
        tempMedicationPlan.get().setStartDate(medicationPlan.getStartDate());
        tempMedicationPlan.get().setEndDate(medicationPlan.getEndDate());
        return medicationPlanRepository.save(tempMedicationPlan.get()).getId();
    }

    public UUID addMedicationToPlan(UUID id, UUID medicationId) {
        MedicationPlan medicationPlan = MedicationPlanBuilder.toEntity(this.findById(id));
        Medication medication = MedicationBuilder.toEntity(medicationService.findMedicationById(medicationId));
        if(!medicationPlan.getMeds().contains(medication)) {
            medicationPlan.getMeds().add(medication);
            update(medicationPlan);
        }
        return medicationPlan.getId();
    }
}
