package ro.tuc.ds2020.services;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.entities.Activity;
import ro.tuc.ds2020.repositories.ActivityRepository;

import java.util.UUID;

@Service
public class ActivityService {

    public static final Logger LOGGER = LoggerFactory.getLogger(ActivityService.class);

    private final ActivityRepository activityRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository){
        this.activityRepository = activityRepository;
    }

    public UUID insert(Activity activity){
        activity = activityRepository.save(activity);
        LOGGER.debug("Activity to patient with id {} was inserted in database", activity.getPatient_id());
        return activity.getPatient_id();
    }

    public void delete(Activity activity){
        activityRepository.delete(activity);
    }
}

