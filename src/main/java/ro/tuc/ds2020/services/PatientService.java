package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PatientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;

    @Autowired
    private MedicationPlanService medicationPlanService;

    @Autowired
    public PatientService(PatientRepository patientRepository) {this.patientRepository = patientRepository; }

    public List<PatientDTO> findPatients(){
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    public PatientDTO findPatientById(UUID id){
        Optional<Patient> prosumerOptional = patientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return PatientBuilder.toPatientDTO(prosumerOptional.get());
    }

    public Patient findPatientByIdNonDTO(UUID id){
        Patient patient = null;
        try {
            patient = patientRepository.findById(id).orElseThrow(NoSuchFieldException::new);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
//        if (!patient.isPresent()) {
//            LOGGER.error("Patient with id {} was not found in db", id);
//            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
//        }
        return patient;
    }

    public Patient findByUsername(String username){
        Optional<Patient> patient = patientRepository.findByUsername(username);
        if(!patient.isPresent()){
            LOGGER.error("Patient with username {} was not found in db", username);
//            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with username: " + username);
            return null;
        }
        return patient.get();
    }

    public UUID insert(PatientDTO patientDTO){
        Patient patient = PatientBuilder.toEntity(patientDTO);
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    public UUID update(PatientDTO patientDTO){
        Patient patient = findPatientByIdNonDTO(patientDTO.getId());
        patient.setName(patientDTO.getName());
        patient.setAddress(patientDTO.getAddress());
        patient.setGender(patientDTO.getGender());
        patient.setAge(patientDTO.getAge());
        patient.setMedRec(patientDTO.getMedRec());
        patient.setPassword(patientDTO.getPassword());
        patient.setUsername(patientDTO.getUsername());
//        patientRepository.deleteById(patientDTO.getId());
        return patientRepository.save(patient).getId();
    }

    public UUID addMedicationPlan(UUID id, UUID mpId) {
        Patient patient = findPatientByIdNonDTO(id);
        if(!patient.getMedicationPlans().contains(medicationPlanService.findById(mpId))){
            patient.getMedicationPlans().add(MedicationPlanBuilder.toEntity(medicationPlanService.findById(mpId)));
            update(PatientBuilder.toPatientDTO(patient));
        }
        return patient.getId();
    }

    public void delete(UUID id){
        patientRepository.deleteById(id);
    }

}
