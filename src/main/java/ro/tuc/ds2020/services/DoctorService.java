package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DoctorDTO;
import ro.tuc.ds2020.dtos.builders.DoctorBuilder;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.DoctorRepository;

import javax.print.Doc;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DoctorService {
    public static final Logger LOGGER = LoggerFactory.getLogger(DoctorService.class);
    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository){
        this.doctorRepository = doctorRepository;
    }

    public List<DoctorDTO> findAll(){
        List<Doctor> docsList = doctorRepository.findAll();
        return docsList.stream().map(DoctorBuilder::toDoctorDTO).collect(Collectors.toList());
    }

    public Doctor findByUsername(String username){
        Optional<Doctor> doctor = doctorRepository.findByUsername(username);
        if(!doctor.isPresent()){
            LOGGER.error("Doctor with username {} was not found in db", username);
//            throw new ResourceNotFoundException(Doctor.class.getSimpleName() + " with username: " + username);
            return null;
        }
        return doctor.get();
    }

    public Doctor findById(UUID id){
        Optional<Doctor> doctor = doctorRepository.findById(id);
        if(!doctor.isPresent()){
            LOGGER.error("Doctor with id {} was not found in db", id);
            throw new ResourceNotFoundException(Doctor.class.getSimpleName() + " with id: " + id);
        }
        return doctor.get();
    }

    public UUID insert(DoctorDTO doctorDTO){
        Doctor doctor = DoctorBuilder.toEntity(doctorDTO);
        doctor = doctorRepository.save(doctor);
        LOGGER.debug("Doctor with id {} was inserted in db", doctor.getId());
        return doctor.getId();
    }

    public UUID update(DoctorDTO doctorDTO){
        Doctor doctor = this.findById(doctorDTO.getId());
        doctor.setName(doctorDTO.getName());
        doctor.setAddress(doctorDTO.getAddress());
        doctor.setGender(doctorDTO.getGender());
        doctor.setAge(doctorDTO.getAge());
        doctor.setPassword(doctorDTO.getPassword());
        doctor.setUsername(doctorDTO.getUsername());
        return doctorRepository.save(doctor).getId();
    }

    public void delete(UUID id){
        doctorRepository.deleteById(id);
    }
}
