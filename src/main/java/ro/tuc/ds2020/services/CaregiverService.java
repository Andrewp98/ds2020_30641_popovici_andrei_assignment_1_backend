package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.CaregiverRepository;

import javax.swing.text.Caret;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CaregiverService {
    public static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {this.caregiverRepository = caregiverRepository;}

    public List<CaregiverDTO> findCaregivers(){
        List<Caregiver> caregiverList = caregiverRepository.findAll();
        return caregiverList.stream()
                .map(CaregiverBuilder::toCaregiveDTO)
                .collect(Collectors.toList());
    }

    public CaregiverDTO findCaregiverById(UUID id){
        Optional<Caregiver> prosumerOptional = caregiverRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.toCaregiveDTO(prosumerOptional.get());
    }

    public Caregiver findByUsername(String username){
        Optional<Caregiver> caregiver = caregiverRepository.findByUsername(username);
        if(!caregiver.isPresent()){
            LOGGER.error("Caregiver with username {} was not found in db", username);
//            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with username: " + username);
            return null;
        }
        return caregiver.get();
    }

    public UUID insert(CaregiverDTO caregiverDTO){
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDTO);
        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    public UUID update(CaregiverDTO caregiverDTO){
        Caregiver caregiver = null;
        try {
            caregiver = caregiverRepository.findById(caregiverDTO.getId()).orElseThrow(NoSuchFieldException::new);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        caregiver.setName(caregiverDTO.getName());
        caregiver.setAddress(caregiverDTO.getAddress());
        caregiver.setAge(caregiverDTO.getAge());
        caregiver.setGender(caregiverDTO.getGender());
        caregiver.setPatList(caregiverDTO.getPatList());
//        caregiverRepository.deleteById(caregiverDTO.getId());
        return caregiverRepository.save(caregiver).getId();
    }

    public void delete(UUID id){
        caregiverRepository.deleteById(id);
    }
}
