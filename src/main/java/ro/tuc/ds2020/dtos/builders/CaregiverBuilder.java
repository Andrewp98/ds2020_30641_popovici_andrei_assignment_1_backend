package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.entities.Caregiver;

import javax.swing.text.Caret;

public class CaregiverBuilder {

    public CaregiverBuilder() {
    }

    public static CaregiverDTO toCaregiveDTO(Caregiver caregiver){
        return new CaregiverDTO(caregiver.getId(),
                caregiver.getName(),
                caregiver.getAge(),
                caregiver.getGender(),
                caregiver.getAddress(),
                caregiver.getUsername(),
                caregiver.getPassword(),
                caregiver.getPatList());
    }

    public static Caregiver toEntity(CaregiverDTO caregiverDTO){
        return new Caregiver(caregiverDTO.getName(),
                caregiverDTO.getAddress(),
                caregiverDTO.getAge(),
                caregiverDTO.getGender(),
                caregiverDTO.getUsername(),
                caregiverDTO.getPassword(),
                caregiverDTO.getPatList());
    }
}
