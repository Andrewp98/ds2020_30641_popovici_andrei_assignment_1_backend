package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;

import java.util.ArrayList;
import java.util.List;

public class MedicationPlanBuilder {

    public MedicationPlanBuilder() {}

    public static MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medicationPlan){
        List<MedicationDTO> medicationList = new ArrayList<>();
        for (Medication medications: medicationPlan.getMeds()){
            medicationList.add(MedicationBuilder.toMedicationDTO(medications));
        }
        return new MedicationPlanDTO(medicationPlan.getId(), medicationPlan.getStartDate(), medicationPlan.getEndDate(), medicationList);
    }

    public static MedicationPlan toEntity(MedicationPlanDTO medicationPlanDTO){
        List<Medication> medicationList = new ArrayList<>();
        for(MedicationDTO med : medicationPlanDTO.getMeds()){
            medicationList.add(MedicationBuilder.toEntity(med));
        }
        return new MedicationPlan(medicationPlanDTO.getId(), medicationPlanDTO.getStartDate(), medicationPlanDTO.getEndDate(), medicationList);
    }
}
