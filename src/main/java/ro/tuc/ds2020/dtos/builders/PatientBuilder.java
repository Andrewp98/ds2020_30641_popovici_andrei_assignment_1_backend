package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Patient;

public class PatientBuilder {

    public PatientBuilder() {
    }

    public static PatientDTO toPatientDTO(Patient patient) {
        return new PatientDTO(patient.getId(),
                patient.getName(),
                patient.getAge(),
                patient.getGender(),
                patient.getAddress(),
                patient.getUsername(),
                patient.getPassword(),
                patient.getMedRec());
    }

    public static Patient toEntity(PatientDTO patientDTO) {
        return new Patient(patientDTO.getName(),
                patientDTO.getAddress(),
                patientDTO.getAge(),
                patientDTO.getGender(),
                patientDTO.getUsername(),
                patientDTO.getPassword(),
                patientDTO.getMedRec());
    }
}
