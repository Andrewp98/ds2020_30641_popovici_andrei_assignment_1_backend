package ro.tuc.ds2020.dtos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class MedicationDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private UUID id;
    private String name;
    private String sideEffects;
    private float dosage;

    public MedicationDTO(){
    }

    public MedicationDTO(UUID id, String name, String sideEffects, float dosage) {
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public float getDosage() {
        return dosage;
    }

    public void setDosage(float dosage) {
        this.dosage = dosage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDTO that = (MedicationDTO) o;
        return Float.compare(that.dosage, dosage) == 0 &&
                Objects.equals(name, that.name) &&
                Objects.equals(sideEffects, that.sideEffects);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sideEffects, dosage);
    }
}
