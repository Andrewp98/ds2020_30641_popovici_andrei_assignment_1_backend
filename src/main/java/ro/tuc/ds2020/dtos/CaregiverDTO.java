package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.LoginDetails;
import ro.tuc.ds2020.entities.Patient;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class CaregiverDTO extends PersonDTO {

    private List<Patient> patList;

    public CaregiverDTO() {
        super();
        this.setType("caregiver");
    }

    public CaregiverDTO(List<Patient> patList) {
        this.patList = patList;
        this.setType("caregiver");
    }

    public CaregiverDTO(UUID id, String name, int age, List<Patient> patList) {
        super(id, name, age);
        this.patList = patList;
        this.setType("caregiver");
    }

    public CaregiverDTO(UUID id, String name, int age, String gender, List<Patient> patList) {
        super(id, name, age, gender);
        this.setType("caregiver");
        this.patList = patList;
    }

    public CaregiverDTO(UUID id, String name, int age, String gender, String address, String username, String password, List<Patient> patList) {
        super(id, name, age, gender, address, username, password);
        this.patList = patList;
        this.setType("caregiver");
    }


    public List<Patient> getPatList() {
        return patList;
    }

    public void setPatList(List<Patient> patList) {
        this.patList = patList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CaregiverDTO that = (CaregiverDTO) o;
        return Objects.equals(patList, that.patList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), patList);
    }
}
