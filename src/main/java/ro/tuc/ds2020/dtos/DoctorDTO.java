package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.LoginDetails;

import java.util.UUID;

public class DoctorDTO extends PersonDTO {

    public DoctorDTO() {
        super();
        this.setType("doctor");
    }

    public DoctorDTO(UUID id, String name, int age) {
        super(id, name, age);
        this.setType("doctor");
    }

    public DoctorDTO(UUID id, String name, int age, String gender) {
        super(id, name, age, gender);
        this.setType("doctor");
    }

    public DoctorDTO(UUID id, String name, int age, String gender, String address, String username, String password) {
        super(id, name, age, gender, address, username, password);
        this.setType("doctor");
    }
}
