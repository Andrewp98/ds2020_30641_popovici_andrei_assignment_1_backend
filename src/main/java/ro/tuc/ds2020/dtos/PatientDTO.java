package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.LoginDetails;
import ro.tuc.ds2020.entities.MedicationPlan;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class PatientDTO extends PersonDTO {

    private String medRec;

    private List<MedicationPlan> medicationPlans;

    public PatientDTO() {
        super();
        this.setType("patient");
        this.setMedicationPlans(new ArrayList<>());
    }

    public PatientDTO(String medRec) {
        this.medRec = medRec;
        this.setType("patient");
        this.setMedicationPlans(new ArrayList<>());
    }

    public PatientDTO(UUID id, String name, int age, String medRec) {
        super(id, name, age);
        this.medRec = medRec;
        this.setType("patient");
    }

    public PatientDTO(String medRec, List<MedicationPlan> medicationPlans) {
        this.medRec = medRec;
        this.medicationPlans = medicationPlans;
    }

    public PatientDTO(UUID id, String name, int age, String gender, String medRec, List<MedicationPlan> medicationPlans) {
        super(id, name, age, gender);
        this.medRec = medRec;
        this.medicationPlans = medicationPlans;
    }

    public PatientDTO(UUID id, String name, int age, String gender, String medRec) {
        super(id, name, age, gender);
        this.medRec = medRec;
        this.setType("patient");
    }

    public PatientDTO(UUID id, String name, int age, String gender, String address, String medRec, List<MedicationPlan> medicationPlans) {
        super(id, name, age, gender, address);
        this.medRec = medRec;
        this.medicationPlans = medicationPlans;
    }

    public PatientDTO(UUID id, String name, int age, String gender, String address, String username, String password, String medRec, List<MedicationPlan> medicationPlans) {
        super(id, name, age, gender, address, username, password);
        this.medRec = medRec;
        this.medicationPlans = medicationPlans;
    }

    public PatientDTO(UUID id, String name, int age, String gender, String address, String username, String password, String medRec) {
        super(id, name, age, gender, address, username, password);
        this.medRec = medRec;
        this.setType("patient");
        this.setMedicationPlans(new ArrayList<>());
    }

    public String getMedRec() {
        return medRec;
    }

    public void setMedRec(String medRec) {
        this.medRec = medRec;
    }

    public List<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(List<MedicationPlan> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PatientDTO that = (PatientDTO) o;
        return Objects.equals(medRec, that.medRec);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), medRec);
    }
}
