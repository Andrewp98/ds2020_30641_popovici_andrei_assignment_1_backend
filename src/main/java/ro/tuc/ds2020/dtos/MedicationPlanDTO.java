package ro.tuc.ds2020.dtos;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import ro.tuc.ds2020.entities.Medication;

import javax.persistence.CascadeType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class MedicationPlanDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private UUID id;
    private String startDate;
    private String endDate;
    List<MedicationDTO> meds;

    public MedicationPlanDTO(String startDate, String endDate, List<MedicationDTO> meds) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.meds = meds;
    }

    public MedicationPlanDTO(UUID id, String startDate, String endDate, List<MedicationDTO> meds) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.meds = meds;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<MedicationDTO> getMeds() {
        return meds;
    }

    public void setMeds(List<MedicationDTO> meds) {
        this.meds = meds;
    }
}
