package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.DoctorBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.LoginDetails;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.CaregiverService;
import ro.tuc.ds2020.services.DoctorService;
import ro.tuc.ds2020.services.PatientService;

import java.util.ArrayList;

@RestController
@RequestMapping("login")
public class LoginControlller {

    @Autowired
    private PatientService patientService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private CaregiverService caregiverService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> validateLogin(@RequestBody LoginDetails loginForm) throws JsonProcessingException {
        String username = loginForm.getUsername();
        String password = loginForm.getPassword();

        Patient patient = patientService.findByUsername(username);
        Doctor doctor = doctorService.findByUsername(username);
        Caregiver caregiver = caregiverService.findByUsername(username);

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

        if(patient != null){
            if(patient.getPassword().equals(password)){
                return new ResponseEntity<>(ow.writeValueAsString(PatientBuilder.toPatientDTO(patient)), HttpStatus.OK);
            }
        }

        if(doctor != null){
            if(doctor.getPassword().equals(password)){
                return new ResponseEntity<>(ow.writeValueAsString(DoctorBuilder.toDoctorDTO(doctor)), HttpStatus.OK);
            }
        }

        if(caregiver != null){
            if(caregiver.getPassword().equals(password)){
                return new ResponseEntity<>(ow.writeValueAsString(CaregiverBuilder.toCaregiveDTO(caregiver)), HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(ow.writeValueAsString(new PersonDTO()), HttpStatus.BAD_REQUEST);
    }
}
