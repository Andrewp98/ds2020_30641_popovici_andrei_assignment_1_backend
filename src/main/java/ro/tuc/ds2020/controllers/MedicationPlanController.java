package ro.tuc.ds2020.controllers;


import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.services.MedicationPlanService;
import ro.tuc.ds2020.services.MedicationService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationPlan")
public class MedicationPlanController {

    private final MedicationPlanService medicationPlanService;
    public static final Logger LOGGER = LoggerFactory.getLogger(MedicationPlanController.class);

    public MedicationPlanController(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }


    //findALL
    @GetMapping()
    public ResponseEntity<List<MedicationPlanDTO>> findAll() {
        List<MedicationPlanDTO> dtos = medicationPlanService.findAll();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    //findById
    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationPlanDTO> findById(@PathVariable("id") UUID medicationPlanId) {
        MedicationPlanDTO dto = medicationPlanService.findById(medicationPlanId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    //insert
    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@RequestBody MedicationPlan medicationPlan) {
        UUID personID = medicationPlanService.insert(medicationPlan);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    //add medication
    @PostMapping("/toPlan")
    public ResponseEntity<UUID> insertMedicaiton(@RequestBody ObjectNode objectNode){
        String strId = objectNode.get("mid").asText();
        String strMpid = objectNode.get("mpid").asText();
        UUID id = UUID.fromString(strId);
        UUID mpid = UUID.fromString(strMpid);
        UUID respID = medicationPlanService.addMedicationToPlan(id, mpid);
        return  new ResponseEntity<>(respID, HttpStatus.CREATED);
    }

    //TODO: UPDATE, DELETE per resource

    @PutMapping()
    public ResponseEntity<UUID> updateProsumer(@RequestBody MedicationPlan medicationPlan){
        UUID personID = medicationPlanService.update(medicationPlan);
        return new ResponseEntity<>(personID, HttpStatus.NO_CONTENT);
    }

    @DeleteMapping()
    public ResponseEntity<UUID> deleteProsumer(@RequestBody MedicationPlanDTO medicationPlanDTO){
        medicationPlanService.delete(medicationPlanDTO.getId());
        return new ResponseEntity(medicationPlanDTO.getId(), HttpStatus.ACCEPTED);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handle(HttpMessageNotReadableException e) {
        LOGGER.warn("Returning HTTP 400 Bad Request", e);
    }

}

