package ro.tuc.ds2020.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.services.CaregiverService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    //findPatients


    //findALL
    @GetMapping()
    public ResponseEntity<List<CaregiverDTO>> findAll() {
        List<CaregiverDTO> dtos = caregiverService.findCaregivers();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    //findById
    @GetMapping(value = "/{id}")
    public ResponseEntity<CaregiverDTO> findById(@PathVariable("id") UUID caregiverId) {
        CaregiverDTO dto = caregiverService.findCaregiverById(caregiverId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    //insert
    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody CaregiverDTO caregiverDTO) {
        UUID personID = caregiverService.insert(caregiverDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    //TODO: UPDATE, DELETE per resource

    @PutMapping()
    public ResponseEntity<UUID> updateProsumer(@RequestBody CaregiverDTO caregiverDTO){
        UUID personID = caregiverService.update(caregiverDTO);
        return new ResponseEntity<>(personID, HttpStatus.NO_CONTENT);
    }

    @DeleteMapping()
    public ResponseEntity<UUID> deleteProsumer(@RequestBody CaregiverDTO caregiverDTO){
        caregiverService.delete(caregiverDTO.getId());
        return new ResponseEntity(caregiverDTO.getId(), HttpStatus.ACCEPTED);
    }
}
