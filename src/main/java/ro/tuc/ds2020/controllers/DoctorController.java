package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.DoctorDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.builders.DoctorBuilder;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.services.DoctorService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;


@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    private final DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService){
        this.doctorService = doctorService;
    }

    //findALL
    @GetMapping()
    public ResponseEntity<List<DoctorDTO>> findAll() {
        List<DoctorDTO> dtos = doctorService.findAll();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    //findById
    @GetMapping(value = "/{id}")
    public ResponseEntity<DoctorDTO> findById(@PathVariable("id") UUID doctorId) {
        Doctor doctor = doctorService.findById(doctorId);
        DoctorDTO dto = DoctorBuilder.toDoctorDTO(doctor);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    //insert
    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody DoctorDTO doctorDTO) {
        UUID personID = doctorService.insert(doctorDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    //TODO: UPDATE, DELETE per resource

    @PutMapping()
    public ResponseEntity<UUID> updateProsumer(@RequestBody DoctorDTO doctorDTO){
        UUID personID = doctorService.update(doctorDTO);
        return new ResponseEntity<>(personID, HttpStatus.NO_CONTENT);
    }

    @DeleteMapping()
    public ResponseEntity<UUID> deleteProsumer(@RequestBody DoctorDTO doctorDTO){
        doctorService.delete(doctorDTO.getId());
        return new ResponseEntity(doctorDTO.getId(), HttpStatus.ACCEPTED);
    }
}
