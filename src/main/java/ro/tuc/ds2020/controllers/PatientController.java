package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.services.DoctorService;
import ro.tuc.ds2020.services.PatientService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;
    public static final Logger LOGGER = LoggerFactory.getLogger(PatientController.class);

    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }


    //findALL
    @GetMapping()
    public ResponseEntity<List<PatientDTO>> findAll() {
        List<PatientDTO> dtos = patientService.findPatients();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    //findById
    @GetMapping(value = "/{id}")
    public ResponseEntity<PatientDTO> findById(@PathVariable("id") UUID patientId) {
        PatientDTO dto = patientService.findPatientById(patientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    //insert
    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@RequestBody PatientDTO patientDTO) {
        UUID personID = patientService.insert(patientDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    //add medication
    @PostMapping("/addMed")
    public ResponseEntity<UUID> insertMedicaiton(@RequestBody ObjectNode objectNode){
        String strId = objectNode.get("id").asText();
        String strMpid = objectNode.get("mpid").asText();
        UUID id = UUID.fromString(strId);
        UUID mpid = UUID.fromString(strMpid);
        UUID respID = patientService.addMedicationPlan(id, mpid);
        return  new ResponseEntity<>(respID, HttpStatus.CREATED);
    }


    //TODO: UPDATE, DELETE per resource

    @PutMapping()
    public ResponseEntity<UUID> updateProsumer(@RequestBody PatientDTO patientDTO){
        UUID personID = patientService.update(patientDTO);
        return new ResponseEntity<>(personID, HttpStatus.NO_CONTENT);
    }

    @DeleteMapping()
    public ResponseEntity<UUID> deleteProsumer(@RequestBody PatientDTO patientDTO){
        patientService.delete(patientDTO.getId());
        return new ResponseEntity(patientDTO.getId(), HttpStatus.ACCEPTED);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handle(HttpMessageNotReadableException e) {
        LOGGER.warn("Returning HTTP 400 Bad Request", e);
    }

}
