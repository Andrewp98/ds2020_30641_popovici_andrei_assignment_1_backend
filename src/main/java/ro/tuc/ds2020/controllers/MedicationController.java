package ro.tuc.ds2020.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.services.MedicationService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;
    public static final Logger LOGGER = LoggerFactory.getLogger(MedicationController.class);

    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }


    //findALL
    @GetMapping()
    public ResponseEntity<List<MedicationDTO>> findAll() {
        List<MedicationDTO> dtos = medicationService.findMedications();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    //findById
    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationDTO> findById(@PathVariable("id") UUID medicationId) {
        MedicationDTO dto = medicationService.findMedicationById(medicationId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    //insert
    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@RequestBody MedicationDTO medicationDTO) {
        UUID personID = medicationService.insert(medicationDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    //TODO: UPDATE, DELETE per resource

    @PutMapping()
    public ResponseEntity<UUID> updateProsumer(@RequestBody MedicationDTO medicationDTO){
        UUID personID = medicationService.update(medicationDTO);
        return new ResponseEntity<>(personID, HttpStatus.NO_CONTENT);
    }

    @DeleteMapping()
    public ResponseEntity<UUID> deleteProsumer(@RequestBody MedicationDTO medicationDTO){
        medicationService.delete(medicationDTO.getId());
        return new ResponseEntity(medicationDTO.getId(), HttpStatus.ACCEPTED);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handle(HttpMessageNotReadableException e) {
        LOGGER.warn("Returning HTTP 400 Bad Request", e);
    }

}
