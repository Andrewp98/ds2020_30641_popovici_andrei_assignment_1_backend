package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Person;

import java.util.Optional;
import java.util.UUID;

public interface PatientRepository extends JpaRepository<Patient, UUID> {
    //Optional<Patient> findByUsername(String username);
//    @Query(value = "SELECT p" +
//            "FROM Person p " +
//            "WHERE p.username = :username ")
    Optional<Patient> findByUsername(String username);
}
