//package ro.tuc.ds2020.consumer;
//
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//
//import org.springframework.amqp.core.*;
//import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
//import org.springframework.amqp.rabbit.connection.ConnectionFactory;
//import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
//import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
//import org.springframework.amqp.support.converter.MessageConverter;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.messaging.handler.annotation.MessageMapping;
//import org.springframework.messaging.handler.annotation.SendTo;
//import org.springframework.messaging.simp.SimpMessagingTemplate;
//import org.springframework.stereotype.Component;
//import ro.tuc.ds2020.controllers.ActivityController;
//import ro.tuc.ds2020.entities.Activity;
//
//@Component
//public class FilteredConsumer {
//
//    @Autowired
//    private SimpMessagingTemplate brokerMessagingTemplate;
//
//    @RabbitListener(queues = "sd_queue")
//    public void consumeMessageFromQueue(Activity activity){
//
//        this.brokerMessagingTemplate.convertAndSend("/alert", activity);
//
//        System.out.println(activity.toString());
//    }
//
//}
